import sys
import json
import operator
import os
import subprocess


with open(sys.argv[1]) as data_file:
	data = json.load(data_file)

if not os.path.exists('Amostras/'):
	os.makedirs('Amostras/')

def preparaAmostra(amostra):
	if not os.path.exists('Amostras/'+amostra['id']):
		os.makedirs('Amostras/'+amostra['id'])
	conf = open('Amostras/'+amostra['id']+'/file.conf','w')
	conf.write("servidor=localhost\n")
	conf.write("usuario=root\n")
	conf.write("senha=cntydswn8oh=\n")
	conf.write("banco=moodle\n")
	conf.write("url=http://200.137.66.6/moodle\n")
	conf.write("token=50d438a6a2e0582727bca6094033c21d\n")
	conf.write("ws-url=http://200.137.66.6/ws/main.php\n")
	conf.write("tags="+amostra['tags']+"\n")
	conf.close()

def executaAmostra(amostra):
	command = "php download.php -d Amostras/{}/data/ --conf Amostras/{}/file.conf".format(amostra['id'],amostra['id'])
	#print(command)
	command = command.split(" ")
	resultado = {}
	resultado['id'] = amostra['id']
	cont = subprocess.run(command,stdout=subprocess.PIPE).stdout.decode('utf-8')
	cont = cont.split('\n')
	
	for out in cont:
		if out:
			out = out.split(":")			
			time  = out[1].split(" ")[1]
			resultado[out[0]] = float(time)
	
	return resultado

#print(data)
tags = {}
for moodle in data:
	for course in data[moodle]:
		for task in data[moodle][course]:
			for student in data[moodle][course][task]:
				for submission in data[moodle][course][task][student]:					
					if len(submission['idnumber']) == 0:
						tag = "NULL"
					else:
						tag = submission['idnumber']
					
					if tag in tags:
						tags[tag] += 1
					else:
						tags[tag] = 1
tags = sorted(tags.items(), key=operator.itemgetter(1))

resultados = []
cont = 1
for i in range(1,len(tags)+1):
	t = ""
	print(i)
	for j in range(0,i):
		t += tags[j][0]+";"
	t = t[:-1]
	#print(t)
	amostra = {}
	amostra['id'] = str(i)
	amostra['tags'] = t
	preparaAmostra(amostra)
	log = executaAmostra(amostra)
	resultados.append(log)
	print("-----------------")
for i in resultados:
	print(i)
