<?php
function hello($name){
	return "Hello $name\n";
}
//function user_files($array, $field1, $value1,$field2,$value2,$url,$token)
function user_files($array, $field1, $value1,$field2,$value2)
{
	$user_submitions = array();
	foreach ($array as $key => $obj){
		$item = get_object_vars($obj);
   		if ( $item[$field1] === $value1 and $item[$field2] === $value2){
   			//$item['url'] = $url."/webservice/pluginfile.php/".$item['contextid']."/assignsubmission_file/submission_files/".$item['itemid']."/".$item['filename']."?forcedownload=1&token=".$token;
		    $aux= array('index' => $key,'item' => $item );
		    array_push($user_submitions, $aux);
		}
   	}
   	return $user_submitions;
}

//function result_query($dominioMoodle,$tokenMoodle){
function result_query($MySQL){

	$sql = "SELECT mdl_course_modules.course, mdl_context.instanceid, mdl_files.userid, mdl_files.contextid, mdl_files.itemid, mdl_files.filename,
mdl_grade_grades.rawgrademin, mdl_grade_grades.rawgrademax, mdl_grade_grades.id as id_grade_grades, mdl_course_modules.idnumber,
IFNULL((SELECT mdl_grade_grades_professor.finalGRADE
FROM mdl_grade_grades as mdl_grade_grades_professor, mdl_user
    WHERE mdl_grade_grades_professor.ID = mdl_grade_grades.id
AND mdl_grade_grades.usermodified  = mdl_user.id
and mdl_user.username <> 'soap'),-1) AS notaProfessor, mdl_course.shortname as course_name, '' as resposta
FROM mdl_files, mdl_context, mdl_course_modules, mdl_grade_items, mdl_grade_grades, mdl_course
WHERE mdl_files.contextid = mdl_context.id
AND mdl_files.component = 'assignsubmission_file'
AND mdl_context.instanceid = mdl_course_modules.id
AND mdl_files.userid = mdl_grade_grades.userid
and mdl_course_modules.instance = mdl_grade_items.iteminstance
and mdl_grade_items.id = mdl_grade_grades.itemid
and mdl_course_modules.course = mdl_course.id
AND filesize > 0
union all
select mdl_course.id as course, mdl_course_modules.id as instanceid, mdl_grade_grades.userid, 0 as contextid, 0 as itemid, '' as filename,
mdl_grade_grades.rawgrademin, mdl_grade_grades.rawgrademax, mdl_grade_grades.id as id_grade_grades, mdl_course_modules.idnumber as idws,
IFNULL((SELECT mdl_grade_grades_professor.finalGRADE
FROM mdl_grade_grades as mdl_grade_grades_professor, mdl_user
    WHERE mdl_grade_grades_professor.ID = mdl_grade_grades.id
AND mdl_grade_grades.usermodified  = mdl_user.id
and mdl_user.username <> 'soap'),-1) AS notaProfessor, mdl_course.shortname as course_name, mdl_assignsubmission_onlinetext.onlinetext as resposta
from mdl_course, mdl_assign, mdl_assign_submission, mdl_assignsubmission_onlinetext, mdl_course_modules, mdl_grade_items, mdl_grade_grades
where mdl_course.id = mdl_assign.course
and mdl_assign.id = mdl_assign_submission.assignment
and mdl_assign_submission.id = mdl_assignsubmission_onlinetext.submission
and mdl_assign.id = mdl_assignsubmission_onlinetext.assignment
and mdl_assign.id = mdl_course_modules.instance
and mdl_assign.id = mdl_grade_items.iteminstance
and mdl_grade_items.id = mdl_grade_grades.itemid
and mdl_assign_submission.userid = mdl_grade_grades.userid
and mdl_course_modules.course = mdl_assign.course
ORDER BY 1,2,3";

$sqlFeed = "SELECT * FROM mdl_assignfeedback_comments";
$sqlNames = "SELECT mdl_user.id, mdl_user.firstname, mdl_user.lastname FROM mdl_user";

$MySQLi = new MySQLi($MySQL['servidor'], $MySQL['usuario'], $MySQL['senha'], $MySQL['banco']);


// Verifica se ocorreu um erro e exibe a mensagem de erro
if (mysqli_connect_errno())
    trigger_error(mysqli_connect_error(), E_USER_ERROR);


// Fim do codigo de configuracao DB

// Executa a consulta OU mostra uma mensagem de erro
$MySQLi->set_charset("utf8");
$result_data = $MySQLi->query($sql) OR trigger_error($MySQLi->error, E_USER_ERROR);
$result_names =  $MySQLi->query($sqlNames) OR trigger_error($MySQLi->error, E_USER_ERROR);
$result_feedbacks = $MySQLi->query($sqlFeed) OR trigger_error($MySQLi->error, E_USER_ERROR);



if($result_data){
	// Cycle through results
	while ($anexo = $result_data->fetch_object()){
		$anexos[] = $anexo;
	}
	// Free result set
	$result_data->close();
}

if($result_names){
  while ($name = $result_names->fetch_object()){
    $names[] = $name;
  }
  // Free result set
  $result_names->close();
}

if($result_feedbacks){
  while ($feedback = $result_feedbacks->fetch_object()){
    $feedbacks[] = $feedback;
  }
  // Free result set
  $result_feedbacks->close();
}



header('Content-type: application/json');
$MySQLi->close();

//return json_encode(array('anexos' => $anexos, 'feedbacks' => $feedbacks));
return json_encode(array('anexos' => $anexos,'users' => $names,'feedbacks' => $feedbacks));
//return "OK";



}



function atualizaQuestoes($sql,$questoesJson)
{
	$input = json_decode($questoesJson);
 	// para cada questao

	for ($i = 0; $i < count($input); $i++)
	{
		//$input[$i]->id_grade_grades;
		atualizaQuestao($sql,$input[$i]->id_grade_grades, $input[$i]->nota, 0, $input[$i]->feedback);
		//array_push($aux,$out);
	}

	//se tudo correr bem retorna true, caso contrario uma lista de erros, que o chamador registrará no log;
	return "OK";
}

function atualizaQuestao ($MySQL,$id, $nota, $professor, $feedback)
{
	//global $MySQLi;
	/*
	$MySQL = array('servidor' => 'localhost',   // EndereÃ§o do servidor
	    'usuario' => 'root',     // UsuÃ¡rio
	    'senha' => 'cntydswn8oh=',               // Senha
	    'banco' => 'moodle'        // Nome do banco de dados
	);*/

	$MySQLi = new MySQLi($MySQL['servidor'], $MySQL['usuario'], $MySQL['senha'], $MySQL['banco']);

	//return $id;
	if ($nota >= 0)
	{
		if(is_null($feedback)){
			echo "$id sem feedback\n";
			$query = "UPDATE mdl_grade_grades SET RAWGRADE = '".$MySQLi->real_escape_string($nota)."'
			,finalgrade = '".$MySQLi->real_escape_string($nota)."'
			,timemodified = unix_timestamp(now())
			,usermodified = (select id from mdl_user where username = 'wsmoodle')
			WHERE ID = '".$MySQLi->real_escape_string($id)."'
			AND (usermodified not in (SELECT mdl_role_assignments.userid FROM mdl_role_assignments, mdl_context, mdl_course, mdl_grade_items WHERE mdl_context.contextlevel = 50
									AND mdl_role_assignments.contextid = mdl_context.id
									AND mdl_context.instanceid = mdl_course.id
									AND mdl_course.id = mdl_grade_items.courseid
									AND mdl_role_assignments.roleid in (3,4)
									AND mdl_grade_items.id = mdl_grade_grades.itemid)
				OR rawgrade is null)";

		}else{
			echo "$id com feedback\n";
			$query = "UPDATE mdl_grade_grades SET RAWGRADE = '".$MySQLi->real_escape_string($nota)."'
				,finalgrade = '".$MySQLi->real_escape_string($nota)."'
				,feedback = '".$MySQLi->real_escape_string($feedback)."'
				,feedbackformat = 1
				,timemodified = unix_timestamp(now())
				,usermodified = (select id from mdl_user where username = 'wsmoodle')
				WHERE ID = '".$MySQLi->real_escape_string($id)."'
				AND (usermodified not in (SELECT mdl_role_assignments.userid FROM mdl_role_assignments, mdl_context, mdl_course, mdl_grade_items WHERE mdl_context.contextlevel = 50
										AND mdl_role_assignments.contextid = mdl_context.id
										AND mdl_context.instanceid = mdl_course.id
										AND mdl_course.id = mdl_grade_items.courseid
										AND mdl_role_assignments.roleid in (3,4)
										AND mdl_grade_items.id = mdl_grade_grades.itemid)
					OR rawgrade is null)";

		}


	}else{
		if(!is_null($feedback)){
			echo "$id Feedback sem nota\n";
			$query = "UPDATE mdl_grade_grades SET feedback = '".$MySQLi->real_escape_string($feedback)."',
			feedbackformat = 1,
			timemodified = unix_timestamp(now()),
        	usermodified = (select id from mdl_user where username = 'wsmoodle')WHERE ID = '".$MySQLi->real_escape_string($id)."'              AND (usermodified not in (SELECT mdl_role_assignments.userid FROM mdl_role_assignments, mdl_context, mdl_course, mdl_grade_items WHERE mdl_context.contextlevel = 50 AND mdl_role_assignments.contextid = mdl_context.id AND mdl_context.instanceid = mdl_course.id  AND mdl_course.id = mdl_grade_items.courseid AND mdl_role_assignments.roleid in (3,4) AND mdl_grade_items.id = mdl_grade_grades.itemid)  OR rawgrade is null)";
		}

	}

	if( $MySQLi->query($query) )
	{

		//Fazer um select na tabela mdl_assign_grades, se existir registro, fazer um update, senão fazer um insert
		$sql = "SELECT mdl_assign_grades.id
		FROM mdl_grade_grades, mdl_grade_items, mdl_assign_grades
		where mdl_grade_grades.id = '".$MySQLi->real_escape_string($id)."'
		and mdl_grade_grades.itemid = mdl_grade_items.id
		and mdl_grade_items.iteminstance = mdl_assign_grades.assignment
		and mdl_grade_grades.userid = mdl_assign_grades.userid
		and mdl_grade_items.itemmodule = 'assign'";

		// Executa a consulta OU mostra uma mensagem de erro
		$resultado = $MySQLi->query($sql);
		$Count = $resultado->fetch_row();

		$resultado->close();

		$id_assign_grades = $Count[0];

		// Se existir registro, fazer update
		if($id_assign_grades > 0)
		{
			//$dados = $resultado->fetch_assoc();
			if ($nota >= 0)
			{
				$query = "UPDATE mdl_assign_grades SET timemodified = unix_timestamp(now())
				,grader = (select id from mdl_user where username = 'wsmoodle')
				,grade = '".$MySQLi->real_escape_string($nota)."'
				WHERE ID = ".$id_assign_grades;
			}
			else
			{
				$query = "UPDATE mdl_assign_grades SET timemodified = unix_timestamp(now()
				,grader = (select id from mdl_user where username = 'wsmoodle')
				WHERE ID = ".$id_assign_grades;
			}

			$MySQLi->query($query);

			// Altera comentário
			$query = "UPDATE mdl_assignfeedback_comments
			SET commenttext = '".$MySQLi->real_escape_string($feedback)."'
			WHERE grade = ".$id_assign_grades;

			$MySQLi->query($query);
		}
		// Se não existir registro, fazer insert
		else
		{
			if ($nota >= 0)
			{
				$query = "INSERT INTO mdl_assign_grades (assignment, userid, timecreated, timemodified, grader, grade, attemptnumber)
				VALUES (
						(SELECT mdl_grade_items.iteminstance
						FROM  mdl_grade_grades, mdl_grade_items
						where mdl_grade_grades.id = '".$MySQLi->real_escape_string($id)."'
						and mdl_grade_grades.itemid = mdl_grade_items.id
						and mdl_grade_items.itemmodule = 'assign'),
						(SELECT userid FROM mdl_grade_grades WHERE ID = '".$MySQLi->real_escape_string($id)."'),
						unix_timestamp(now()),
						unix_timestamp(now()),
						(select id from mdl_user where username = 'wsmoodle'),
						'".$MySQLi->real_escape_string($nota)."',
						0)";
			}
			else
			{
				$query = "INSERT INTO mdl_assign_grades (assignment, userid, timecreated, timemodified, grader, attemptnumber)
				VALUES (
						(SELECT mdl_grade_items.iteminstance
						FROM  mdl_grade_grades, mdl_grade_items
						where mdl_grade_grades.id = '".$MySQLi->real_escape_string($id)."'
						and mdl_grade_grades.itemid = mdl_grade_items.id
						and mdl_grade_items.itemmodule = 'assign'),
						(SELECT userid FROM mdl_grade_grades WHERE ID = '".$MySQLi->real_escape_string($id)."'),
						unix_timestamp(now()),
						unix_timestamp(now()),
						(select id from mdl_user where username = 'wsmoodle'),
						0)";
			}
			$MySQLi->query($query);

			// Cria registro para feedback - inicio

			$sql = "SELECT mdl_assign_grades.id
			FROM mdl_grade_grades, mdl_grade_items, mdl_assign_grades
			where mdl_grade_grades.id = '".$MySQLi->real_escape_string($id)."'
			and mdl_grade_grades.itemid = mdl_grade_items.id
			and mdl_grade_items.iteminstance = mdl_assign_grades.assignment
			and mdl_grade_grades.userid = mdl_assign_grades.userid
			and mdl_grade_items.itemmodule = 'assign'";

			$resultado = $MySQLi->query($sql);
			$Count = $resultado->fetch_row();
			$resultado->close();
			$id_assign_grades = $Count[0];

			$query = "INSERT INTO mdl_assignfeedback_comments (commenttext, assignment, grade, commentformat)
			VALUES (
					'".$MySQLi->real_escape_string($feedback)."',
					(SELECT mdl_grade_items.iteminstance
					FROM  mdl_grade_grades, mdl_grade_items
					where mdl_grade_grades.id = '".$MySQLi->real_escape_string($id)."'
					and mdl_grade_grades.itemid = mdl_grade_items.id
					and mdl_grade_items.itemmodule = 'assign'),".$id_assign_grades.",1)";

			$MySQLi->query($query);

			// Cria registro para feedback - fim
		}

		return "Nota Atualizada.";
	}
	else
	{
		return "Database Error: Unable to update record.";
	}
}
?>
